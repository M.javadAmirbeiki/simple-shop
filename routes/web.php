<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'CommonController@home');

Route::get('/order/{product_id}', 'OrderController@insert');

Route::get('/cart','OrderController@cartContent');

Route::get('/order/remove/{product_id}','OrderController@removeItem');
