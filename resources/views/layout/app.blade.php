<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Simple shop</title>
    <link href="/css/app.css" rel="stylesheet">
    @yield("meta-head")
</head>
<body>
@include("layout.navbar")
@yield("content")
@include("layout.footer")
<script src="{{ mix('js/app.js') }}"></script>
@yield("script")
</body>
</html>
