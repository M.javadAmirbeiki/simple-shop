@extends("layout.app")
@section("content")
    <div class="bg-white">
        <div class="max-w-2xl mx-auto py-5 px-4 sm:py-5 sm:px-6 lg:max-w-7xl lg:px-8">
            <div class="relative overflow-x-auto shadow-md sm:rounded-lg">
                <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
                    <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                    <tr>
                        <th scope="col" class="px-6 py-3">
                            Product name
                        </th>
                        <th scope="col" class="px-6 py-3">
                            Count
                        </th>
                        <th scope="col" class="px-6 py-3">
                            Category
                        </th>
                        <th scope="col" class="px-6 py-3">
                            Price
                        </th>
                        <th scope="col" class="px-6 py-3">
                            <span class="sr-only">Remove</span>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($items as $key => $item)
                    <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                        <th scope="row" class="px-6 py-4 font-medium text-gray-900 dark:text-white whitespace-nowrap">
                            {{ $item[0]->product->name }}
                        </th>
                        <td class="px-6 py-4">
                            {{ count($item) }}
                        </td>
                        <td class="px-6 py-4">
                            {{ $item[0]->product->category->title }}
                        </td>
                        @if($item[0]->product->off)
                            <td class="px-6 py-4">
                                ${{ count($item) * (($item[0]->product->price * $item[0]->product->off) / 100) }}
                            </td>
                        @else
                        <td class="px-6 py-4">
                            ${{ count($item) * ($item[0]->product->price) }}
                        </td>
                        @endif
                        <td class="px-6 py-4 text-right">
                            <a href="/order/remove/{{ $item[0]->product_id }}" class="font-medium text-blue-600 dark:text-blue-500 hover:underline">Remove</a>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
