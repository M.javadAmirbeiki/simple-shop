<div class="group relative">
    <div class="w-full min-h-80 bg-gray-200 aspect-w-1 aspect-h-1 rounded-md overflow-hidden group-hover:opacity-75 lg:h-80 lg:aspect-none">
        <img src="{{ $product->image }}" alt="{{ $product->name }}" class="w-full h-full object-center object-cover lg:w-full lg:h-full">
    </div>
    <div class="mt-4 flex justify-between">
        <div>
            <h3 class="text-sm text-gray-700">
                {{--                                <a href="">--}}
                {{--                                    <span aria-hidden="true" class="absolute inset-0"></span>--}}
                {{ $product->name }}
                {{--                                </a>--}}
            </h3>
            @if($product->off)
                <small class="text-gray-500">with <span class="text-red-500">{{ $product->off }}%</span> discount : {{ ($product->price * $product->off) / 100 }}</small>
            @endif
        </div>
        <p class="text-sm font-medium text-gray-900 @if($product->off) line-through @endif">${{ $product->price }}</p>

    </div>
    <a href="/order/{{ $product->id }}">
    <button style="margin-top: 5px;width: 100%" class="bg-white hover:bg-gray-100 text-gray-800 font-semibold py-2 px-4 border border-gray-400 rounded shadow">
        Order Now!
    </button>
    </a>
</div>
