@extends("layout.app")
@section("meta-head")
@endsection
@section("content")
    <div id="app">
        <slider :slides="{{ json_encode($slides) }}"/>
    </div>
    @if(session()->has("error"))
        <x-components.alert type="error" :text="session()->get('error')"/>
    @elseif(session()->has("success"))
        <x-components.alert type="success" :text="session()->get('success')"/>
    @endif
    <div class="bg-white">
        <div class="max-w-2xl mx-auto py-5 px-4 sm:py-5 sm:px-6 lg:max-w-7xl lg:px-8">
            <h2 class="text-2xl font-extrabold tracking-tight text-gray-900">Most Viewed Products :</h2>

            <div class="mt-6 grid grid-cols-1 gap-y-10 gap-x-6 sm:grid-cols-2 lg:grid-cols-4 xl:gap-x-8">
                @foreach($most_viewed as $product)
                    <x-components.product :product="$product"/>
                @endforeach

                <!-- More products... -->
            </div>
        </div>
    </div>
    @foreach($categories_products as $category)
    @if(count($category->products) == 0)
        @continue
    @endif
    <div class="bg-white">
        <div class="max-w-2xl mx-auto py-5 px-4 sm:py-5 sm:px-6 lg:max-w-7xl lg:px-8">
            @if(!empty($category->parent))
                <small>{{ $category->parent->title }}</small>
            @endif
            <h2 class="text-2xl font-extrabold tracking-tight text-gray-900">{{ $category->title }} :</h2>

            <div class="mt-6 grid grid-cols-1 gap-y-10 gap-x-6 sm:grid-cols-2 lg:grid-cols-4 xl:gap-x-8">
                @foreach($category->products as $product)
                    <x-components.product :product="$product"/>
                @endforeach
                <!-- More products... -->
            </div>
        </div>
    </div>
    @endforeach
@endsection
