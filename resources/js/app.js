require('./bootstrap');

import { createApp } from 'vue'

import VueSplide from '@splidejs/vue-splide';

import Slider from './components/Slider.vue';

const app = createApp({});

app.component('slider', Slider)

app.use( VueSplide );

app.mount('#app')
