<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class Order extends Model
{
    use HasFactory;

    public function items() {
        return $this->hasMany(OrderItem::class);
    }

    /**
     * Author : MJavad Amirbeiki
     * Description : view section must be transformed to middleware!
     * @param Product $product
     * @param $ip
     * @return bool
     */
    public function addToCart(Product $product,$ip) {
        $last_day = Carbon::today()->subDay();
        $order = Order::where("ip_address",$ip)->where('created_at','>=',$last_day)->first();
        $view = $product->views()->where("ip_address",$ip)->first();
        if (!$order) {
            $order = $this;
            $order->ip_address = $ip;
        }
        DB::beginTransaction();
        try {
            $order->save();
            $item = new OrderItem;
            $item->product_id = $product->id;
            $order->items()->save($item);
            if (!$view) {
                $view = new View;
                $view->ip_address = $ip;
                $product->views()->save($view);
            }
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            return false;
        }
        return true;
    }
}
