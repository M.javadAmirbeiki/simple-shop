<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DB;

class OrderController extends Controller
{
    public function insert(Request $request,$product_id) {
        $product = Product::findOrFail($product_id);
        $order = new Order;
        if (!$order->addToCart($product,$request->ip()))
            return redirect()->back()->with("error","Server Problem");
        return redirect()->back()->with("success","Order Added To Cart!");
    }

    public function cartContent(Request $request) {
        $last_day = Carbon::today()->subDay();
        $order = Order::where("ip_address",$request->ip())->where('created_at','>=',$last_day)->first();
        if (!$order)
            return redirect('/')->with("error","Cart is empty!");
        $items = $order->items()->with("product.category")->get();
        if (count($items) == 0)
            return redirect('/')->with("error","Cart is empty!");
        $items = $items->groupBy("product_id");
        return view("cart",compact("items"));
    }

    public function removeItem(Request $request,$product_id) {
        $last_day = Carbon::today()->subDay();
        $order = Order::where("ip_address",$request->ip())->where('created_at','>=',$last_day)->firstOrFail();
        $order->items()->where("product_id",$product_id)->delete();
        return redirect()->back()->with("success","Item Removedّ");
    }
}
