<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use App\Models\Slide;
use App\Models\View;
use Illuminate\Http\Request;
use DB;

class CommonController extends Controller
{
    public function home() {
        $slides = $this->renderSlides(Slide::all());
        $most_viewed = View::select('product_id',DB::raw('count(*) as total'))->orderBy("total")->groupBy("product_id")->limit(4)->pluck("product_id");
        $most_viewed = Product::whereIn("id",$most_viewed)->get();
        $categories_products = Category::with("products")->get();
        return view("index",compact("slides","categories_products","most_viewed"));
    }

    /**
     * Author : MJavad Amirbeiki
     * Description : rendering slides as img tag
     * @param $slides
     * @return array
     */
    public function renderSlides($slides) {
        $result = [];
        foreach ($slides as $slide) {
            $result[] = "<img src='$slide->image' alt='$slide->title'>";
        }
        return $result;
    }
}
