<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use DB;

class SlideSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('slides')->insert([
            'title' => "image 1",
            'image' => "https://via.placeholder.com/1920x480/0000?text=First%20Image",
        ]);
        DB::table('slides')->insert([
            'title' => "image 2",
            'image' => "https://via.placeholder.com/1920x480/0000?text=Second%20Image",
        ]);
    }
}
