<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use DB;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'id' => '1',
            'title' => "First Category",
        ]);
        DB::table('categories')->insert([
            'id' => '2',
            'title' => "Second Category",
        ]);
        DB::table('categories')->insert([
            'id' => '3',
            'title' => "Third Category",
            'parent_id' => '1'
        ]);
    }
}
