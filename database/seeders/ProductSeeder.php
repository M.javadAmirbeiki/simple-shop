<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            'name' => "First Product",
            'image' => "https://tailwindui.com/img/ecommerce-images/product-page-01-related-product-01.jpg",
            'description' => "Lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content. Lorem ipsum may be used as a placeholder before final copy is available.",
            'price' => "100000",
            'category_id' => "2",
        ]);
        DB::table('products')->insert([
            'name' => "Second Product",
            'image' => "https://tailwindui.com/img/ecommerce-images/product-page-01-related-product-02.jpg",
            'description' => "Lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content. Lorem ipsum may be used as a placeholder before final copy is available.",
            'price' => "150000",
            'category_id' => "3",
            'off' => "15",
        ]);
    }
}
